module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: ['airbnb-base', 'plugin:prettier/recommended'],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
  plugins: ['prettier', '@typescript-eslint'],
  ignorePatterns: ['**/dist', '**/*.pdf'],
  rules: {
    'prettier/prettier': ['error'],
    'import/no-named-as-default': 0,
    'func-names': 0,
    'no-console': 0,
    'no-unused-vars': 0,
    'import/extensions': [
      'error',
      'ignorePackages',
      {
        js: 'never',
        jsx: 'never',
        ts: 'never',
        tsx: 'never',
      },
    ],
  },
  settings: {
    'import/resolver': {
      node: {
        extensions: ['.js', '.jsx', '.ts', '.tsx'],
      },
    },
  },
  overrides: [
    {
      files: ['tests/**/*.ts'],
      rules: {
        'no-unused-expressions': 'off',
      },
    },
  ],
}
