# Unit tests skills assessment

Skills assessments project for Mocha, Chai and Sinon

### Installation

`yarn install`

### Instructions

1. Write unit tests for all files in the ./tests directory
2. Tests must cover 100% of the code in the ./src folder
3. Use sinon & proxyquire to stub or mock all 3rd party libraries

### Usage

Start (Development) - `yarn dev`  
Start (Production) - `yarn start`  
Build - `yarn build`  
Unit tests - `yarn test`  
Coverage - `yarn coverage`

© 2022 Jersey Dev Inc. All rights reserved
