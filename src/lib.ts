import { Request, Response, NextFunction } from 'express'
import fs from 'fs'

const onStart = () => {
  console.log('\x1b[32m%s\x1b[0m', 'Express server started! Ready for requests.')
}

const sendNotFound = (req: Request, res: Response) => {
  res.status(404).send({ message: 'Not found' })
}

const getErrorStatusCode = (err: any) => {
  let statusCode = 500
  if (err.response && err.response.status) {
    statusCode = err.response.status
  } else if (err.statusCode) {
    statusCode = err.statusCode
  } else if (err.status) {
    statusCode = err.status
  }
  return statusCode
}

const getError = (error: any) => {
  let jsonError
  if (error.response) {
    jsonError = error.response.data
  } else if (Array.isArray(error)) {
    jsonError = JSON.stringify(error)
    jsonError = JSON.parse(jsonError)
  } else {
    jsonError = JSON.stringify(error, Object.getOwnPropertyNames(error))
    jsonError = JSON.parse(jsonError)
  }

  delete jsonError.status
  delete jsonError.statusCode

  return jsonError
}

const sendError = (err: any, req: Request, res: Response, next: NextFunction) => {
  console.log(err)
  const statusCode = getErrorStatusCode(err)
  const error = getError(err)
  res.send(error)
}

const writeTxtFileToPublicDir = (fileName: string, data: string): Promise<void> => {
  return new Promise((resolve, reject) => {
    const path = `${__dirname}/../public/${fileName}`
    fs.writeFile(path, data, (err) => {
      if (err) {
        reject(err)
        return
      }

      resolve()
    })
  })
}

export { onStart, sendError, sendNotFound, getError, getErrorStatusCode, writeTxtFileToPublicDir }
