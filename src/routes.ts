import express from 'express'
import { renderReadMe, uploadFile } from './controllers'

const router = express.Router()

router.get('/', renderReadMe)
router.post('/upload', uploadFile)

export default router
