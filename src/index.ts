import express from 'express'
import cors from 'cors'
import path from 'path'
import { engine } from 'express-handlebars'
import { onStart, sendError, sendNotFound } from './lib'
import routes from './routes'

const app = express()
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cors())
app.use(express.static(path.join(__dirname, '../public')))
app.engine('handlebars', engine())
app.set('view engine', 'handlebars')
app.set('views', './views')

app.use('/', routes)
app.listen(process.env.PORT || 3000, onStart)
app.use(sendNotFound)
app.use(sendError)

export default app
