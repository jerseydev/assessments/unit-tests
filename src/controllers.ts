import { Request, Response, NextFunction } from 'express'
import fs from 'fs'
import { marked } from 'marked'
import { writeTxtFileToPublicDir } from './lib'

const renderReadMe = async (req: Request, res: Response) => {
  const file = fs.readFileSync(`${__dirname}/../README.md`, 'utf8')
  res.render('readme', {
    pageTitle: 'Unit tests API',
    content: marked(file.toString()),
  })
}

const uploadFile = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  try {
    await writeTxtFileToPublicDir('myFile.txt', 'My file contents')
    res.status(204).send()
  } catch (e) {
    next(e)
  }
}

export { renderReadMe, uploadFile }
